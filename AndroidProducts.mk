#
# Copyright (C) 2024 The Android Open Source Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_shennong.mk

COMMON_LUNCH_CHOICES := \
    lineage_shennong-eng \
    lineage_shennong-userdebug \
    lineage_shennong-user